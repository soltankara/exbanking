defmodule ExBanking do
  use GenServer

  def create_user(user) do
    start_server()
    user = validate_user(user)

    {user, user_account} = get_user(user)
    cond do
      user_account != nil -> throw(:user_already_exists)
      true -> set_user(user, %{"processCount" => 0}) 
    end
  end

  def deposit(user, amount, currency) do
    {status, _} = start_server()

    if status == :ok do
      throw(:user_does_not_exist)
    end

    user = validate_user(user)
    amount = validate_amount(amount)
    currency = validate_currency(currency)

    {user, user_account} = get_validated_user(user)

    {_, processCount} = get_user_process_count(user)
    if processCount >= 10 do
      throw(:too_many_requests_to_user)
    end

    processCount = processCount + 1
    user_account = Map.put(user_account, "processCount", processCount)
    set_user(user, user_account)

    a = Map.get(user_account, currency)
    
    user_account = 
    case a do
      nil -> Map.put(user_account, currency, amount)
      _ ->  Map.put(user_account, currency, a + amount) 
    end

    processCount = processCount - 1
    user_account = Map.put(user_account, "processCount", processCount)

    set_user(user, user_account)
    {:ok, Map.get(user_account, currency)}
  end
  
  def withdraw(user, amount, currency) do
    {status, _} = start_server()

    if status == :ok do
      throw(:user_does_not_exist)
    end

    user = validate_user(user)
    amount = validate_amount(amount)
    currency = validate_currency(currency)

    {user, user_account} = get_validated_user(user)
    
    {_, processCount} = get_user_process_count(user)
    if processCount >= 10 do
      throw(:too_many_requests_to_user)
    end

    processCount = processCount + 1
    user_account = Map.put(user_account, "processCount", processCount)
    set_user(user, user_account)
   
    a = 
    case Map.get(user_account, currency) do
      nil -> 0.00
      _ -> Map.get(user_account, currency)
    end

    user_account = 
    case a do
      0.00 -> Map.put(user_account, currency, a)
      _ -> user_account
    end
    
    new_amount = a - amount
    if new_amount < 0 do
      throw(:not_enough_money)
    end

    processCount = processCount - 1
    user_account = Map.put(user_account, "processCount", processCount)
    user_account = Map.put(user_account, currency, new_amount)
    set_user(user, user_account)
    
    {:ok, Map.get(user_account, currency)}
  end

  def get_balance(user, currency) do
    {status, _} = start_server()

    if status == :ok do
      throw(:user_does_not_exist)
    end

    user = validate_user(user)
    currency = validate_currency(currency)

    {user, user_account} = get_validated_user(user)

    {_, processCount} = get_user_process_count(user)
    if processCount >= 10 do
      throw(:too_many_requests_to_user)
    end

    processCount = processCount + 1
    user_account = Map.put(user_account, "processCount", processCount)
    set_user(user, user_account)

    balance =
    case Map.get(user_account, currency) do
      nil -> 0.00
      _ -> Map.get(user_account, currency)
    end

    processCount = processCount - 1
    user_account = Map.put(user_account, "processCount", processCount)
    user_account = Map.put(user_account, currency, balance)
    set_user(user, user_account)
    {:ok, balance}
  end

  def send(from_user, to_user, amount, currency) do
    {status, _} = start_server()

    if status == :ok do
      throw(:sender_does_not_exist)
    end

    from_user = validate_user(from_user)
    to_user = validate_user(to_user)
    amount = validate_amount(amount)
    currency = validate_currency(currency)
    
    {fromUser, from_user_account} = get_user(from_user)
    {toUser, to_user_account} = get_user(to_user)

    {_, fromUserProcessCount} = get_user_process_count(from_user)
    {_, toUserProcessCount} = get_user_process_count(to_user)
   
    cond do
      from_user_account == nil -> throw(:sender_does_not_exist)
      to_user_account == nil -> throw(:receiver_does_not_exist)
      fromUserProcessCount >= 10 -> throw(:too_many_requests_to_sender)
      toUserProcessCount >= 10 -> throw(:too_many_requests_to_receiver)
      true ->
        {_, from_user_balance} = withdraw(fromUser, amount, currency)
        {_, to_user_balance} = deposit(toUser, amount, currency)
        {:ok, from_user_balance, to_user_balance}
    end
  end

  def validate_currency(currency) do
    currency = to_string(currency)
    currency
  end

  def validate_user(user) do
    user = to_string(user)
    user
  end

  def get_validated_user(user) do
    {user, user_account} = get_user(user)
    cond do
      user_account == nil -> throw(:user_does_not_exist)
      true -> {user, user_account}
    end
  end

  def validate_amount(amount) do
    cond do
      is_float(amount) == false -> throw(:wrong_arguments)
      true -> 
        amount = :erlang.float_to_binary(amount, [decimals: 2])
        amount = String.to_float(amount)
        amount
      end
  end

  def start_server do
    start_link()
  end

  def start_link do
    GenServer.start_link(__MODULE__, %{}, name: :user_store)
  end

  def init(userDataMap) do
      {:ok, userDataMap}
  end

  def get_user(user) do
      GenServer.call(:user_store, {:get_user, user})
  end

  def set_user(user, data) do
      GenServer.cast(:user_store, {:set_user, user, data})
  end

  def get_user_process_count(user) do
    GenServer.call(:user_store, {:get_user_process_count, user})
  end

  def handle_call({:get_user, user}, _from, user_data) do
      user_account = Map.get(user_data, user)
      {:reply, {user, user_account}, user_data}
  end

  def handle_call({:get_user_process_count, user}, _from, user_data) do
    user_account = Map.get(user_data, user)
    processCount = Map.get(user_account, "processCount")
    {:reply, {user, processCount}, user_data}
  end

  def handle_cast({:set_user, user, data}, user_data) do
      user_data = Map.put(user_data, user, data)
      {:noreply, user_data}
  end
end
